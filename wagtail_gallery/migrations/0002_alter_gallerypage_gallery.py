# Generated by Django 4.0.8 on 2022-10-13 16:18

from django.db import migrations
import wagtail.blocks
import wagtail.fields
import wagtail.images.blocks


class Migration(migrations.Migration):

    dependencies = [
        ('wagtail_gallery', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='gallerypage',
            name='gallery',
            field=wagtail.fields.StreamField([('image', wagtail.blocks.StructBlock([('image', wagtail.images.blocks.ImageChooserBlock()), ('alt', wagtail.blocks.CharBlock(required=False)), ('summary', wagtail.blocks.CharBlock(required=False))]))], use_json_field=True),
        ),
    ]
