from wagtail.admin.panels import FieldPanel
from wagtail.fields import RichTextField, StreamField
from wagtail.models import Page
from .blocks import GalleryStreamBlock


class GalleryIndexPage(Page):
    intro = RichTextField(blank=True)

    subpage_types = [
        'wagtail_gallery.GalleryIndexPage',
        'wagtail_gallery.GalleryPage'
    ]

    content_panels = Page.content_panels + [
        FieldPanel('intro', classname="full")
    ]


class GalleryPage(Page):
    intro = RichTextField(blank=True)
    gallery = StreamField(GalleryStreamBlock(), use_json_field=True)

    parent_page_types = [
        'wagtail_gallery.GalleryIndexPage'
    ]

    content_panels = Page.content_panels + [
        FieldPanel('intro'),
        FieldPanel('gallery'),
    ]

    def __str__(self):
        return repr(self.__dict__)

    class Meta:
        verbose_name = "Gallery Page"
        verbose_name_plural = "Gallery Pages"
