from setuptools import find_packages, setup
from wagtail_gallery.__version__ import __version__

setup(
    name='wagtail_gallery',
    version=__version__,
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        'django>=5.0.0',
        'wagtail>=5.0.0',
    ],
    # https://pypi.org/classifiers/
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Environment :: Plugins',
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'Natural Language :: English',
        'License :: Other/Proprietary License',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
    ],
    url='https://bitbucket.org/strombergweb/wagtail_gallery',
    license='None',
    author='Phillip Stromberg',
    author_email='phillip@4stromberg.com',
    description='A gallery streamfield page plugin for Wagtail',
    zip_safe=False,
)
