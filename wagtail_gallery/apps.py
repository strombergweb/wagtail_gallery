from django.apps import AppConfig


class WagtailGalleryConfig(AppConfig):
    name = 'wagtail_gallery'
    verbose_name = 'Picture Gallery'
    default_auto_field = 'django.db.models.AutoField'
