from wagtail.blocks import StreamBlock, CharBlock, StructBlock
from wagtail.images.blocks import ImageChooserBlock


class ImageBlock(StructBlock):
    image = ImageChooserBlock()
    alt = CharBlock(required=False)
    summary = CharBlock(required=False)


class GalleryStreamBlock(StreamBlock):
    image = ImageBlock()
