from django import template

register = template.Library()


@register.inclusion_tag('tags/gallery_index.html', takes_context=True)
def galleries_list(context, calling_page):
    """
    Retrieves all live pages which are children of the calling page for standard index listing

    :param context:
    :param calling_page:
    :return:
    """
    pages = calling_page.get_children().live()

    return {
        'pages': pages,
        # required by the pageurl tag that we want to use within this template
        'request': context['request'],
    }
